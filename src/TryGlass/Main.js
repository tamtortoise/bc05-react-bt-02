import React, { Component } from 'react'
// Import data json glass 
import { dataGlass } from './dataGlass'
import ListGlass from './ListGlass';
import DetailGlass from './DetailGlass';


export default class Main extends Component {
    state = {
        glassArr: dataGlass,
        detail: dataGlass[0],
      };
      handleChangeDetail = (value) => {
        this.setState({ detail: value });
      };

  render() {
    return (
     <div className="container">
        <div className="title py-4 mx-auto" style={{color:"pink", fontSize:"30px"}}>Try some Glasses My Dude</div>
        <div className="row justify-content-center align-items-center">
          <img
            className="col-5 mr-5 position-relative"
            style={{ width: "400px", height: "400px" }}
            src="./Img/essex.gif"
            alt=""
          />
          <img
            className="col-5 ml-5"
            style={{ width: "400px", height: "400px" }}
            src="./Img/stalin.jpg"
            alt=""
          />
        </div>

        <ListGlass
          handleChangeDetail={this.handleChangeDetail}
          glassArr={this.state.glassArr}
        />

        <DetailGlass detail={this.state.detail} />
      </div>
    )
  }
}
