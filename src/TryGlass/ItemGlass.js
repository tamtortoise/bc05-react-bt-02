import React, { Component } from 'react'

export default class ItemGlass extends Component {
    render() {
      let { url } = this.props.data;
      return (
        <div className="col-2 py-4 px-4">
          <div>
            <img
              style={{ width: "90%" }}
              src={url}
              alt="/"
              onClick={() => {
                this.props.handleViewDetail(this.props.data);
              }}
            />
          </div>
        </div>
      );
    }
}