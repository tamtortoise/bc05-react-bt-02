import React, { Component } from 'react'
import ItemGlass from './ItemGlass'


export default class ListGlass extends Component {
    renderListGlass = () => {
      return this.props.glassArr.map((item, index) => {
        return (
          <ItemGlass
            key={index}
            handleViewDetail={this.props.handleChangeDetail}
            data={item}
          />
        );
      });
    };
    render() {
      return <div className="row">{this.renderListGlass()}</div>;
    }
  }
  