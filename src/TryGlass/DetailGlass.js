import React, { Component } from 'react'

export default class DetailGlass extends Component {
    render() {
      return (
        <div>
          <div
            className="relative"
            style={{
              width: "300px",
              marginTop:"-52.75vh",
              marginLeft: "36vh",
              backgroundColor: "transparent",
              textAlign: "center",
              position: "absolute",
            }}
          >
            <img style={{
                marginTop: "-33.75vh",
                marginLeft: "13.5vh",
                width: "30vh",
                height: "9vh",
                opacity: "0.9",
              }}
              src={this.props.detail.url}
              alt=""
            />

            {/* Text box */}
            <p style={{ color: "red", fontSize: "25px" }}>
              {this.props.detail.name}
            </p>
            <p style={{ fontSize: "15px", fontWeight: "bold" }}>
              {this.props.detail.desc}
            </p>
          </div>
        </div>
      );
    }
  }
  