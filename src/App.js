import "./App.css";
import Main from "./TryGlass/Main";

function App() {
  return (
    <div className="App" style={{backgroundImage:"url(./Img/synthwave.jpg)" , objectFit:"fill"}}>
      <Main />
    </div>
  );
}

export default App;
